#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <iterator>
#include <algorithm>
#include <string_view>
#include <chrono>
#include <list>
#include <memory>

using TimePoint = std::chrono::time_point<std::chrono::system_clock>;

template <typename Iter>
struct IWriter
{
    virtual void write(Iter, Iter, TimePoint) = 0;
};

template <typename Iter>
class CliWriter : public IWriter<Iter>
{
    public:
        CliWriter() = default;
        void write(Iter, Iter, TimePoint) override;
};

template <typename Iter>
class FileWriter : public IWriter<Iter>
{
    public:
        FileWriter() = default;

        void write(Iter, Iter, TimePoint) override;
};

template <typename Iter>
class MultiWriter : public IWriter<Iter>
{
    public:
        using IWriterPtr = std::unique_ptr<IWriter<Iter>>;

        MultiWriter() = delete;

        template <class U>
        MultiWriter(U u)
        {
            m_writers.push_back(std::move(u));
        }

        template <class T, class ...Ts>
        MultiWriter(T t, Ts... ts) : MultiWriter(std::forward<Ts>(ts)...)
        {
            m_writers.push_back(std::move(t));
        }

        void write(Iter, Iter, TimePoint) override;

    private:
        std::list<IWriterPtr> m_writers;
};

template <typename Iter>
void CliWriter<Iter>::write(Iter begin, Iter end, TimePoint)
{
    if (begin != end)
    {
        std::cout << "bulk: ";
        std::copy(begin, end - 1, std::ostream_iterator<std::string_view>(std::cout, ", "));
        std::copy(end - 1, end, std::ostream_iterator<std::string_view>(std::cout, " "));
        std::cout << std::endl;
    }
}

template <typename Iter>
void FileWriter<Iter>::write(Iter begin, Iter end, TimePoint tp)
{
    if (begin != end)
    {
        auto epoch_time = std::chrono::duration_cast<std::chrono::seconds>(
                tp.time_since_epoch()).count();

        std::string filename = "bulk" + std::to_string(epoch_time) + ".log";
        std::ofstream m_ofs{filename};

        std::copy(begin, end, std::ostream_iterator<std::string_view>(m_ofs, "\n"));
    }
}

template <typename Iter>
void MultiWriter<Iter>::write(Iter begin, Iter end, TimePoint tp)
{
    for (const auto &writer : m_writers)
    {
        writer->write(begin, end, tp);
    }
}
