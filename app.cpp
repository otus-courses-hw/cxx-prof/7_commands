#include "app.hpp"
#include <stdexcept>

App::App(std::size_t size) :
    inner_block_cmd{0},
    m_handler(new ProcessCmdStatic()),
    m_buff(new Buffer<Container<Commands>>(size)),
    m_writer(nullptr)
{
}

void App::set_writer(IWriterPtr writer)
{
    m_writer = writer;
}

void App::add_cmd(std::string cmd)
{
    m_buff->add(cmd);
    m_handler->add_cmd(this);
}

void App::add_block()
{
    m_handler->add_block(this);
}

void App::del_block()
{
    m_handler->del_block(this);
}

void App::process_cmd()
{
    auto when_block_started = m_buff->time();
    m_writer->write(m_buff->cbegin(), m_buff->cend(), when_block_started);
    m_buff->clear();
}

void App::end()
{
    m_handler->end(this);
    throw std::domain_error("user interrupted input by sending EOF");
}
