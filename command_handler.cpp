#include "command_handler.hpp"
#include "app.hpp"

void ProcessCmdStatic::add_cmd(App *app)
{
    if (app->m_buff->used() == app->m_buff->threshold())
        app->process_cmd();
}

void ProcessCmdStatic::add_block(App *app)
{
    app->process_cmd();
    app->inner_block_cmd++;
    app->m_handler = App::ICmdUserHandlerPtr{new ProcessCmdDynamic()};
}

void ProcessCmdStatic::del_block(App *app)
{
    //noting to do
}

void ProcessCmdStatic::end(App *app)
{
    app->process_cmd();
}

void ProcessCmdDynamic::add_cmd(App *app)
{
    //nothing to do, just collect cmds
}

void ProcessCmdDynamic::add_block(App *app)
{
    app->inner_block_cmd++;
}

void ProcessCmdDynamic::del_block(App *app)
{
   app->inner_block_cmd--;

   if (app->inner_block_cmd == 0)
   {
       app->process_cmd();
       app->m_handler = App::ICmdUserHandlerPtr{new ProcessCmdStatic()};
   }
}

void ProcessCmdDynamic::end(App *app)
{
    app->m_buff->clear();
    app->m_handler = App::ICmdUserHandlerPtr{new ProcessCmdStatic()};
}

