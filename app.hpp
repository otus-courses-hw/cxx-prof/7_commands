#ifndef APP_HPP
#define APP_HPP

#include <stdexcept>
#include <string>
#include <vector>
#include <memory>

#include "writer.hpp"
#include "command_handler.hpp"
#include "buffer.hpp"



class App
{
    public:
        App() = delete;
        App(std::size_t);

        friend class ProcessCmdStatic;
        friend class ProcessCmdDynamic;

        using Commands = std::string;

        template <typename T>
        using Container = std::vector<T>;

        using BuffPtr = std::shared_ptr<BufferBase<Container<Commands>>>;

        using BuffIter = Container<Commands>::const_iterator;
        
        using IWriterPtr = std::shared_ptr<IWriter<BuffIter>>;
        
        using ICmdUserHandlerPtr = std::unique_ptr<ICmdUserHandler>;

        void set_writer(IWriterPtr);
        void add_cmd(std::string);
        void add_block();
        void del_block();
        void end() noexcept(false);
        void process_cmd();

    private:
        std::size_t inner_block_cmd;
        ICmdUserHandlerPtr m_handler;
        BuffPtr m_buff;
        IWriterPtr m_writer;
};

#endif /*APP_HPP*/
