#include <iostream>
#include <memory>
#include <unordered_map>
#include <string>
#include <functional>
#include "app.hpp"

struct IReader
{
    virtual void add_cmd(const std::string&) const  = 0;
    virtual std::istream &add_block(std::istream&) const = 0;
    virtual std::istream &del_block(std::istream&) const = 0;
    virtual std::istream &end(std::istream&) const = 0;

};

using AppPtr = std::shared_ptr<App>;

class CliReader : public IReader
{
    public:
        CliReader(AppPtr);
        void add_cmd(const std::string&) const override;
        std::istream &add_block(std::istream&) const override;
        std::istream &del_block(std::istream&) const override;
        std::istream &end(std::istream&) const override;
        friend std::istream& operator>>(std::istream&, const CliReader&);

    private:
        AppPtr app;
        std::unordered_map<char, std::function<void(std::istream&)>> ctrl_map;

        std::istream &ignore(std::istream&) const;

};
