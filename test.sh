#!/bin/bash

./bulk <<'EOF'
cmd1
cmd2
cmd3
first cmd after buffer flushed
{
d cmd1
d cmd2
d cmd3
d cmd4
{
d cmd41
{
d cmd411
}
d cmd42
}
d cmd5
}
end of user's input
EOF

for file in $(ls *log); do
    echo $file
    cat $file
done
