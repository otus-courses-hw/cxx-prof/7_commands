#include "reader.hpp"
#include <limits>
#include <iostream>

CliReader::CliReader(AppPtr app_)
{
    app = app_;

    using namespace std::placeholders;

    ctrl_map['{'] = std::bind(&CliReader::add_block, this, _1);
    ctrl_map['}'] = std::bind(&CliReader::del_block, this, _1);
    ctrl_map[EOF] = std::bind(&CliReader::end, this, _1);
}

void CliReader::add_cmd(const std::string& cmd) const
{
    app->add_cmd(cmd);
}

std::istream &CliReader::add_block(std::istream& stream) const
{
    app->add_block();
    return ignore(stream);
}

std::istream &CliReader::del_block(std::istream& stream) const
{
    app->del_block();
    return ignore(stream);
}

std::istream &CliReader::end(std::istream& stream) const
{
    app->end();
    ignore(stream).clear();
    clearerr(stdin);
    return stream;
}

std::istream &operator>>(std::istream &stream, const CliReader &reader)
{
    char c = stream.peek();
    if (reader.ctrl_map.find(c) != reader.ctrl_map.end())
    {
        reader.ctrl_map.at(c)(stream);
    }
    else
    {
        std::string token;
        std::getline(stream, token);
        reader.add_cmd(token);
    }
    return stream;
}

std::istream &CliReader::ignore(std::istream &stream) const
{
    stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return stream;
}
