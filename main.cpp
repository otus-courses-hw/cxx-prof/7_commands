#include "app.hpp"
#include "reader.hpp"
#include "writer.hpp"
#include <chrono>
#include <memory>
#include <stdexcept>
#include <thread>

int main()
{
    std::shared_ptr<App> app(new App(3));

    using iter_type = typename App::BuffIter;
    std::shared_ptr<IWriter<iter_type>> writer(new MultiWriter<iter_type>(
                std::move(std::make_unique<CliWriter<iter_type>>()),
                std::move(std::make_unique<FileWriter<iter_type>>())));

    app->set_writer(writer);

    std::shared_ptr<const CliReader> reader(new CliReader(app));
    while(std::cin)
    {
        try
        {
            std::cin >> *reader;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        catch(std::domain_error ex)
        {
            return 0; //user sent EOF
        }
    }

    return 0;
}
